package com.ninestack.notification.firebase;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static com.ninestack.notification.utils.UtilityClass.PREFERENCE;


/**
 * Created by Adrian Almeida.
 */
public class MyFirebaseInstanceIdService  extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIdService";

    // Get updated InstanceID token.
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }


    private void sendRegistrationToServer(String token) {
        setPreference(getApplicationContext(), token);
    }

    static public boolean setPreference(Context c, String token) {
        SharedPreferences settings = c.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("FCM_token", "" + token);
        editor.putBoolean("FCM_token_synced", false);
        LocalBroadcastManager.getInstance(c).sendBroadcast(new Intent("token_update"));
        return editor.commit();
    }
}
