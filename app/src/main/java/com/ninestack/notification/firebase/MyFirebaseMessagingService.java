package com.ninestack.notification.firebase;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ninestack.notification.MainActivity;
import com.ninestack.notification.R;
import com.ninestack.notification.utils.GlideApp;


import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Adrian Almeida.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    int NOTIFICATION_ID = 1;
    String imgUrl = "";
    private String title, content;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        NOTIFICATION_ID = new Random().nextInt();
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "titile test" + remoteMessage.getData().get("message"));
        Log.e(TAG, "Notification Message message: " + remoteMessage.getData().get("title"));

        Log.e(TAG, "image url :" + remoteMessage.getData().get("image"));


        if (isAppIsInBackground(getApplicationContext())) {
            /**
             * App is in background. Shows Notification
             */
            imgUrl = remoteMessage.getData().get("image");
            if (imgUrl != null && !imgUrl.isEmpty())
                shwNotify(remoteMessage);
            else if (remoteMessage.getNotification().getBody()!=null){
                Intent intent_two = new Intent("action.service.to.activity");
                intent_two.putExtra("notificationTitle", remoteMessage.getNotification().getBody());
                intent_two.putExtra("type", remoteMessage.getData().get("type"));
                sendBroadcast(intent_two);
                generateNotification(remoteMessage,null,false);
            }
            Log.i(TAG, "onCreateView: FCM Service app  is in background");
        } else {
            imgUrl = remoteMessage.getData().get("image");
            if (imgUrl != null && !imgUrl.isEmpty())
                shwNotify(remoteMessage);
            else {
                Log.i(TAG, "onCreateView: foreground");
                Intent intent = new Intent("action.service.to.activity");
                intent.putExtra("notificationTitle", remoteMessage.getNotification().getBody());
                intent.putExtra("type", remoteMessage.getData().get("type"));
                sendBroadcast(intent);
                generateNotification(remoteMessage, null,false);
            }

        }
    }

    //Show notification with image
    void shwNotify(final RemoteMessage remoteMessage) {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                GlideApp.with(getApplicationContext())
                        .asBitmap()
                        .load(imgUrl)
                        .transition(GenericTransitionOptions.with(R.anim.alpha))
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                Log.e(TAG, "Notification image downloaded");
                                generateNotification(remoteMessage, resource, true);
                            }
                        });
            }
        });

    }

    /**
     * Data notification
     *
     * @param remoteMessage
     * @param bitmap
     */
    private void generateNotification(RemoteMessage remoteMessage, Bitmap bitmap, boolean dataNotification) {
        Notification notification;
        Intent showIntent = new Intent(getApplicationContext(), MainActivity.class);
        showIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        showIntent.putExtra(getPackageName(), NOTIFICATION_ID); // this line sets off closing notification in onCreate()
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, showIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        String id = "com.notificationTemplate.notification";
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            CharSequence name = "Notification Template";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel mChannel = new NotificationChannel(id, name, importance);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{0, 1000});
            mNotificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), id);
        if (remoteMessage.getData() != null && dataNotification) {
            /**example
             *"data": {"message": "Test","content":"Testing","image": "https://s-i.huffpost.com/gen/945903/images/o-RED-BULL-LAWSUIT-facebook.jpg"}
             */
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> receivedMap = remoteMessage.getData();
            title = receivedMap.get("message");
            content = receivedMap.get("content");
            mBuilder.setContentTitle(title)
                    .setContentText(content);

        } else {
            mBuilder.setContentTitle(remoteMessage.getNotification().getTitle())
                    .setContentText(remoteMessage.getNotification().getBody());

        }

        mBuilder.setLights(Color.WHITE, 500, 500)
                .setDefaults(Notification.DEFAULT_ALL)
                .setColor(getResources().getColor(R.color.colorPrimaryDark))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(getNotificationIcon())
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH);


        mBuilder.setContentIntent(contentIntent);

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        if (bitmap != null) {
            Log.e(TAG, "Notification image set");
            mBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                    .bigPicture(bitmap));
        }
        notification = mBuilder.build();
        NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_ID, notification);


    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.notification_icon : R.mipmap.ic_launcher;//notification icon and app icon
    }


    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


}
