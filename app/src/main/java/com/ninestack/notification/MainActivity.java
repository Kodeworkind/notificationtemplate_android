package com.ninestack.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

public class MainActivity extends AppCompatActivity {


    BroadcastReceiver notificationTokenRefresh = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            registerDevice();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //firebase token
        Log.d("Firebase", "token "+ FirebaseInstanceId.getInstance().getToken());

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(notificationTokenRefresh, new IntentFilter("token_update"));
    }

    //register token to server
    private void registerDevice() {
        String fcmToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("Firebase", "token " + fcmToken);

        if (fcmToken != null)
            registerTokenToServer(fcmToken);

    }

    /**
     * Api call
     * @param fcmToken firebase token
     */
    private void registerTokenToServer(String fcmToken) {
        //api call

    }


}
