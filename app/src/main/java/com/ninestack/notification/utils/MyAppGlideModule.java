package com.ninestack.notification.utils;

/**
 * Created by Vaibhav Barad on 9/30/2017.
 */

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {}