Important Note:
You should send data payload in your FCM message. 
Data payload gets received in on message method irrespective of your app being in foreground or background.

-https://github.com/firebase/quickstart-android/issues/84
Large icons not applied in FCM notification when app is not opened or is in background 


-Using 
  <meta-data android:name="com.google.firebase.messaging.default_notification_icon"
            android:resource="@drawable/notification_icon" />
in manifiest is necessary where @drawable/notification_icon is a icon with transparent background



//NOT yet implemented
Grouping notification 
https://stackoverflow.com/questions/36058887/setgroup-in-notification-not-working?answertab=active#tab-top
https://stackoverflow.com/questions/27309037/stacking-notifications-does-not-work

You have to create a separate group notification and set the group summary flag true only for that,
and that becomes the parent notification that bundles other notifications with the same group key within itself.

Note:
-If a notification with the same tag and id has already been posted by your application and has not yet been canceled, 
it will be replaced by the updated information.














Sending push notification
{
 "registration_ids" : [
 	"eB2MK_60vDU:APA91bE4fV-SCHWiQoXjuPFHYwCaqOnG4CiSPSMGlfGyYqivm8dfgqdd_ib9b_56xuCXL0EoAf-S570quybDX2_RjQpJApGhjiiTbtncuJbN0WCMOHJ8Qpnq_IyxMxKg5qAvgUe8aTOmPChmnEEr1F8GPtSgDNjTJA"
 	],
"notification":{
      "title":"Portugal vs. Denmark",
      "body":"great match!"
    },

  
}





Sending data payload 
{
 "registration_ids" : [
 	"eB2MK_60vDU:APA91bE4fV-SCHWiQoXjuPFHYwCaqOnG4CiSPSMGlfGyYqivm8dfgqdd_ib9b_56xuCXL0EoAf-S570quybDX2_RjQpJApGhjiiTbtncuJbN0WCMOHJ8Qpnq_IyxMxKg5qAvgUe8aTOmPChmnEEr1F8GPtSgDNjTJA"
 	],



    "data": {
    "message": "Test",
    "content":"Testing",
    "image": "https://s-i.huffpost.com/gen/945903/images/o-RED-BULL-LAWSUIT-facebook.jpg" 
   
    }
  
}





